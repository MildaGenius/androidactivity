﻿using UnityEngine;
using UnityEngine.UI;
using JniUtil;

public class TestApp : MonoBehaviour
{
    [SerializeField]
    private Text _textLabel;

    public void OnButtonPressed()
    {
        // Make the call using JNI to the Java Class and write out the response (or write 'Invalid Response From JNI' if there was a problem).
        _textLabel.text = Util.StaticCall("sayHello", "Invalid Response From JNI", "com.aimobile.activityplugin.MyActivity");

    }
}
