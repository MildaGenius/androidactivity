package com.aimobile.activityplugin;

import android.os.Bundle;
import com.unity3d.player.UnityPlayerActivity;

public class MyActivity extends UnityPlayerActivity {

  public void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    System.out.println("Crating MyActivity");
  }

  public static String sayHello()
  {
    return "Hello world from JNI";
  }
}